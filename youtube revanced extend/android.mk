LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := Youtube
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := Youtube.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APP
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_MODULE_PATH := $(TARGET_OUT)/priv-app
LOCAL_OVERRIDES_PACKAGES := Youtube
include $(BUILD_PREBUILT)